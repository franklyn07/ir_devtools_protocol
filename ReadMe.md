Incident Response Tool - Version using devtools protocol

Files Contained:

## Collected Evidence
Contains the evidence files collected by the produced IR
Tool, during various types of test attacks.

## domMutations
Contains extrapolated mutations produced from running
compareEvidence.sh.

## compareEvidence.sh
Bash script that compares collected evidence against, a
clean version of it, and extrapolates DOM mutations,
irrespective of them being malicious or not. Incident
Responder would then analyse these mutations to deduce
whether they are malicious or not (as proposed in the 
forensic analysis technique produced)

## node_modules
npm packages

## timings
DevTools Portocol Monitoring tool; evidence collection
overheads

## ImplementationComparison
Overheads comparison between the DevTools Protocol
Monitoring Implementation as opposed to the chrome 
extension version of it. Produced using Python script 
Visualiztion.py.
