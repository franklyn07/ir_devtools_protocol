import pandas as pd
from pandas.plotting import table
import matplotlib
matplotlib.rcParams['figure.figsize'] = (16, 12)
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def main():
    timings_debug = pd.read_csv("./log_debug.csv")
    timings_server = pd.read_csv("./log_server.csv")
    df_debug = pd.DataFrame(timings_debug)
    df_server = pd.DataFrame(timings_server)
    
    debug = df_debug.groupby(['DOMSize']).mean()
    server = df_server.groupby(['DOMSize']).mean()
    
    #develop figure
    fig, ax = plt.subplots()
    plt.title('Total Time Taken to Collect Evidence (ms) per DOM Size (nodes)', fontsize = 22)

    ax.set_xlabel("DOM Size (nodes)", fontsize = 16)
    ax.set_ylabel("Total Time to Collect Evidence (ms)", fontsize =16)

    ax.plot(debug.index,debug.totalTime, color='orange', marker= 'o', markersize=11)
    ax.plot(server.index,server.totalTime, color='green', marker= 'x', markersize=11)
    ax.grid()
    ax.label_outer()

    fig.set_size_inches(15,10)
    orange_patch = mpatches.Patch(color= 'orange', label='DevTools Protocol Implementation')
    green_patch = mpatches.Patch(color= 'green', label='Chrome Extension Implementation')
    plt.legend(handles=[orange_patch, green_patch], loc = 'upper left', fontsize = 'large')
    fig.text(0.1,0.07, "Collected On Sunday 13th of May 2019")
    fig.savefig('./ImplementationComparison.png', dpi=100)

main()
