//Resources on devtools protocol
//https://github.com/cyrus-and/chrome-remote-interface/issues/64
//https://chromedevtools.github.io/devtools-protocol/tot/Page
//https://groups.google.com/forum/#!forum/chrome-debugging-protocol

//library to launch google chrome
const chromeLauncher = require('chrome-launcher');

//library to use filesystem through node.js
const fs = require('fs');
const path = require('path');

//decleration to use performance-now package to time functions
var now = require("performance-now");
var start = now();
var end = now();

//library to interact with chrome debugging protocol through node js
const CDP = require('chrome-remote-interface');

async function main() {

	//launch chrome through chrome launcher
	const chrome = await chromeLauncher.launch({ 
		port: 9222,
		startingUrl: 'http://localhost:8080/',
		chromeFlags: [
			'--window-size=1200,800',
			'--auto-open-devtools-for-tabs'
		],
		//this needs to set up to connect to user profile where extensions are installed
		//and also allow extensions to run in chrome launcher
		//note that the directory will differ across platforms
		userDataDir: '/home/franklyn07/.config/google-chrome',
		enableExtensions: true
	});

	//connect CDP object to debugger port which is accessible through chrome.port because of chrome-launcher library
	const protocol = await CDP({ port: chrome.port });

	//decleration of domains that will be used
	const { Runtime, Page, DOM, Debugger, DOMDebugger, Network} = protocol;

	//using promise.all 
	//https://stackoverflow.com/questions/45285129/any-difference-between-await-promise-all-and-multiple-await
	//to use specific parts of devtool protocol we need to enable them
	await Promise.all([Runtime.enable(), Page.enable(), DOM.enable(), Debugger.enable(), Network.enable()]);

	//we allow for page lifecycle events to be emitted
	/*Page.setLifecycleEventsEnabled({enabled: true});

	//as soon as they are allowed to be emitted we start listening for them.
	//there are three types of events we are interested in
	//commit - it is emitted as soon as the page is emitted for the first time
	//init - it is emitted in all subsequent event reloads
	//domcontentloaded - it is emitted as soon as page is fully downloaded from server
	//(domcontentloaded is important since when the event is trigerred, it seems that the node ids
	//kept as reference from previous lifecylce events, stop being relevant. Thus dom mutations were
	//not being caught anymore as soon as domcontentloaded was emitted)
	//https://github.com/cyrus-and/chrome-remote-interface/issues/37
	Page.lifecycleEvent(params=>{
		//whenever one of these events is generated it means that the DOM reference
		//tree has been updated, thus we need to renew the breakpoints
		const events = new Set(['commit', 'init', 'DOMContentLoaded']);
		if(events.has(params.name)){
			const options = {
				depth: -1,
				pierce: true
			}

			DOM.getDocument(options, (error,params)=>{
				if(error){
					console.error(params);
					return;
				}

				let options = {
					nodeId: params.root.nodeId,
					selector: 'html'
				}

				DOM.querySelector(options, (error,params)=>{
					if(error){
						console.error(params);
						return;
					}

					const options = {
						nodeId: params.nodeId,
						type: 'subtree-modified'
					}

					//register break points to catch DOM tree modifications
					DOMDebugger.setDOMBreakpoint(options);
				});

				//register modification on divs
				options = {
					nodeId: params.root.nodeId,
					selector:'div'
				}

				//select all div nodes
				DOM.querySelectorAll(options,(error,params)=>{
					if(error){
						console.error(params);
						return;
					}

					//assign breakpoint to each div
					params.nodeIds.forEach(nodeId =>{
						const options = {
							nodeId: nodeId,
							type: 'attribute-modified'
						}

						//register break points to catch attribute modifications on divs
						DOMDebugger.setDOMBreakpoint(options);
					});

				});
			});
		}
	});*/


	//as soon ad domcontentloaded event is trigerred -- buttons are already downloaded by now
	/*Page.domContentEventFired(()=>{
		// register breakpoints based on events in browser attributed to mouse
		registerMouseEvents();

	});*/

	//register network requests before they are sent
	//this only works when metamask is not installed -- apparently chrome
	//does not allow to intercept requests originated in other extensions
	//https://stackoverflow.com/questions/39823971/how-can-i-capture-traffic-originating-from-my-own-firefox-web-extension
	/*Network.requestWillBeSent(async(params)=>{
		//filter only on post rquests that contain json rpc post data
		//Note that json rpc requests are preferred to be sent using HTTP Post
		//https://www.jsonrpc.org/historical/json-rpc-over-http.html
		if(params.request.method == 'POST'){
			const postData = params.request.postData;
			if (postData.search('jsonrpc')!=-1){
				let evidence = await grabEvidence({'Request': 'jsonrpc'},params);
				await saveEvidence(evidence);
			}
		}
	});*/
	

	//get what caused the breakpoint to trigger in event Info as well as grab normal evidence,
	//save both to file
	/*Debugger.paused(async(eventInfo)=> {
		switch(eventInfo.reason){
			case 'DOM':
				//in real life this should be substituted with name of DApp
				let DAppName = 'localhost';

				//requestor url
				let requestor = eventInfo.callFrames[0].url;

				//if mutation in DOM head was not caused by server, ignore data
				//like this we reduce bloating by trying to filter changes in DOM caused by
				//legitimate DApp server from the ones by external entities
				if (requestor.search(DAppName) == -1){
					let evidence = await grabEvidence({[eventInfo.reason] : eventInfo.data.type},eventInfo);
					saveEvidence(evidence);
				}
				break;
			case 'EventListener':
				let evidence = await grabEvidence({'Event Listener': eventInfo.data.eventName},eventInfo);
				saveEvidence(evidence);
				break;
			default:
				console.error('Unkown Event');
				Debugger.resume();
		}		
		Debugger.resume();
	});*/

	//executes DOM traversal function in browser and returns the evidence package
	async function grabEvidence(event, additionalInfo){
		let func = 
		"function mapDOM(element) {"+
	    	"var treeObject = {};"+
		    "function treeHTML(element, object) {"+
		        "object[\"type\"] = element.nodeName;"+
		        "var nodeList = element.childNodes;"+
		        "if (nodeList != null) {"+
		            "if (nodeList.length) {"+
		                "object[\"content\"] = [];"+
		                "for (var i = 0; i < nodeList.length; i++) {"+
		                    "if (nodeList[i].nodeType == 3) {"+
		                        "if(nodeList[i].nodeValue.length > 5){"+
		                            "object[\"content\"].push(nodeList[i].nodeValue);"+
		                        "}" +                      
		                    "} else {" +
		                        "object[\"content\"].push({});"+
		                        "treeHTML(nodeList[i], object[\"content\"][object[\"content\"].length -1]);"+
		                    "}"+
		                "}"+
		            "}"+
		        "}"+
		        "if (element.attributes != null) {"+
		            "if (element.attributes.length) {"+
		                "object[\"attributes\"] = {};"+
		                "for (var i = 0; i < element.attributes.length; i++) {"+
		                    "if(element.attributes[i].nodeName !== \"style\"){"+
		                        "object[\"attributes\"][element.attributes[i].nodeName] = element.attributes[i].nodeValue;"+
		                    "}"+
		                "}"+
		           " }"+
		        "}"+
		        "if (element.style != null && element.style.length > 0) {"+
		            "object[\"style\"] = {};"+
		            "for (var i = 0; i < element.style.length; i++) {"+
		                "object[\"style\"][element.style[i]] = element.style[element.style[i]];"+
		            "}"+
		        "}"+
		        "if (element.value != null){"+
		            "object[\"value\"] = element.value;"+
		        "}"+
		    "}"+
		    "treeHTML(element, treeObject);"+
		    "return treeObject ;"+
		"}";

		let remoteObject = await Runtime.evaluate({expression:"window.document.documentElement"});
		let result = await Runtime.callFunctionOn({functionDeclaration: func, objectId: remoteObject.result.objectId, arguments: [{objectId: remoteObject.result.objectId}],returnByValue: true});

		let evidence = {
	        'Trigger': event,
	        'TimeStamp': await getTimeStamp(true), 
	        'DOM': result.result.value,
	        'AdditionalInfo': additionalInfo
    	};

    	return evidence;
	};

	//register breakpoints on different types of mouse events
	/*async function registerMouseEvents(){
		DOMDebugger.setEventListenerBreakpoint({eventName: 'click', targetName: 'A'});
		DOMDebugger.setEventListenerBreakpoint({eventName: 'mouseenter', targetName: 'A'});
		DOMDebugger.setEventListenerBreakpoint({eventName: 'mouseleave', targetName: 'A'});
		DOMDebugger.setEventListenerBreakpoint({eventName: 'mousedown', targetName: 'A'});
		DOMDebugger.setEventListenerBreakpoint({eventName: 'mouseup', targetName: 'A'});
	};*/

	//timing technique
	var timings = [];

	//function that times the capture and send evidence functions
	async function timeIt(){
	    //count number of nodes
	    numNodes = await countNodes();
	    for (var i = 0; i<100;i++){
	        var t0 = now();
	        var evidence = await grabEvidence({'test': i}, "Timing Technique");
	        var t1 = now();
	        let captureTime = t1-t0;
	        t0 = now();
	        await saveEvidence(evidence);
	        t1 = now();
	        let savingTime = t1-t0;
	        let record = {
	        	'DOMSize': numNodes,
	            'captureEvidence': captureTime,
	            'saveEvidence': savingTime,
	            'totalTime': captureTime+savingTime
	        };
	        timings.push(record);
	    }
	}

	//function that counts number of nodes
	async function countNodes(){
		let func = 
		"function countNodes(element) {"+
		    "let number_nodes = 1;"+
		    "function treeHTML(element) {"+
		        "var nodeList = element.childNodes;"+
		        "if (nodeList != null) {"+
		            "if (nodeList.length) {"+
		                "for (var i = 0; i < nodeList.length; i++) {"+
		                    "number_nodes += 1;"+
		                    "treeHTML(nodeList[i]);"+
		                "}"+
		            "}"+
		        "}"+
		    "}"+
		    "treeHTML(element);"+

		    "return number_nodes;"+
		"}";

		let remoteObject = await Runtime.evaluate({expression:"window.document.documentElement"});
		let result = await Runtime.callFunctionOn({functionDeclaration: func, objectId: remoteObject.result.objectId, arguments: [{objectId: remoteObject.result.objectId}],returnByValue: true});

		return result.result.value;
	}

	//function that increases the present number of nodes in the DOM Tree by 200 nodes
	async function enlargeTheDOM(){
		let func = 
		"function enlargeTheDOM(){"+
		    "for(var i = 0 ; i < 200; i++){"+
		        "var p = document.createElement(\"p\");"+
		        "window.document.body.appendChild(p);"+
		    "}"+
		"}";

		let remoteObject = await Runtime.evaluate({expression:"window.document.documentElement"});
		await Runtime.callFunctionOn({functionDeclaration: func, objectId: remoteObject.result.objectId});
	}


	//function that drives the timing mechanisms
	async function timeDriver(){
	    for(var i = 0; i<25; i++){
	        await timeIt();
	        await enlargeTheDOM();
	    }
	    //create file stream to be able to append timings to it
		//this approach was used since it is more efficient than using append.
		//append creates a handle each time and since we will be appending multiple times
		//would create a whole mess wrt. file system handling.
		//https://stackoverflow.com/questions/3459476/how-to-append-to-a-file-in-node/43370201#43370201
		var stream = fs.createWriteStream("./timings/log.csv", {flags:'a'});
		stream.write("DOMSize,captureEvidence,saveEvidence,totalTime\n");

		console.log(timings);

		//sorts values of each sample object and writes it to file
		timings.forEach(object =>{
			let stringifiedSortedObject = object.DOMSize.toString()+','+object.captureEvidence.toString()+','+object.saveEvidence.toString()+','+object.totalTime.toString();
			console.log(stringifiedSortedObject);
			stream.write(stringifiedSortedObject+'\n');
		});

		stream.end();
		console.log('finished');
	}

	//start timing after everything has loaded -- wait 10seconds
	setTimeout(timeDriver,15000);

}

//get current time details -- json format or stringified json
async function getTimeStamp(json){
    let d = new Date();
    let days = ["Sunday","Monday","Teusday","Wendesday","Thursday","Friday","Saturday"];
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let time = {
        'Day': days[d.getDay()],
        'Month': months[d.getMonth()],
        'Date': d.getDate(),
        'Year': d.getFullYear(),
        'Time': d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() + ':' + d.getMilliseconds(),
        'TimeZone': 'UTC'+d.getTimezoneOffset()/60
    };
    return (json) ? time : JSON.stringify(time);
}

//save evidence 
async function saveEvidence(data){
	let evidence = JSON.stringify(data, null, 2);

	let fileName = await parseFileName(data.TimeStamp);
	let folderName = await parseFolderName(await getTimeStamp(true));

	let directory = './collectedEvidence/'+folderName+'/';
	

	//check if today's data has already started being collected
	//if yes start saving in it else create it and start saving to it
	try{
		fs.statSync(directory);
		//writing evidence to file
		fs.writeFile(directory+fileName+'.json', evidence, () => console.log('Evidence Saved'));
	}catch(e){
		fs.mkdirSync(directory);
		//writing evidence to file
		fs.writeFile(directory+fileName+'.json', evidence, () => console.log('Evidence Saved'));
	}
}

//get timestamp object and parse today's Date into a folder name
function parseFolderName(timestamp){
	let date = timestamp.Date + '_' + timestamp.Month + '_' + timestamp.Year;
	return date;
}

//get timestamp of evidence and parse it into a file name
function parseFileName(ts){
	return ts.Day+'_'+ts.Date.toString()+'_'+ts.Month+'_'+ts.Year.toString()+'_'+ts.Time;
}


main()

