#!/bin/bash

if [ "$1" == '' ];then
	echo 'CMD Missing Date Of Evidence Collected -- Should be in the form: dd_mm_yr'
else
	#check if directory exists -- if it does not, create it
	mkdir -p ./domMutations/"$1"/;
	
	#start making comparisons and store them in appropriate directory
	for filename in ./collectedEvidence/"$1"/*.json;do
		#get time from file contents
		TIME=$(sed -n 10p "$filename")
		PARSEDTIME=$(echo $TIME | egrep -o '[0-9].*[0-9]')
		diff -d "$filename" ./collectedEvidence/clean.json > ./domMutations/"$1"/"$1_$PARSEDTIME";
	done
fi


